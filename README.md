# accounting-api

REST API that exposes information from Jetstream accounting database to users, admins, and other services

## Running for development purposes

```
cp accounting-api.conf.default accounting-api.conf
# customize accounting-api.conf for your cloud
# create a new Python virtual environment and activate it
virtualenv venv
source venv/bin/activate
pip install -r requirements.txt
python3 app/main.py
# Forward local TCP port so that it can connect to database:
ssh -L 127.0.0.1:3306:172.16.8.104:3306 j2m1.jetstream.iu.edu
# to test it:
curl http://127.0.0.1:5000/ -H "X-Auth-Token: redacted"
```

## Running for production (in a Docker container)

```
cp accounting-api.conf.default accounting-api.conf
# customize accounting-api.conf for your cloud, it will be baked into the container
sudo docker build -t acct-api .
docker run -d --name acct-api -p 9001:80 --restart always acct-api
# to test it:
curl http://127.0.0.1:9001 -H "X-Auth-Token: redacted"
# put it behind a reverse proxy
```

## Implementation notes

This was important: <https://github.com/Rackspace-DOT/flask_keystone/issues/6>

## TODO

- [x] Connect to `amie` database
- [x] Figure out how to store DB connection URI in config file
- [x] Need a user for this service on galera cluster with read-only access to `amie` database.

```
CREATE USER "accounting-api" IDENTIFIED BY "secret-password-here";
GRANT SELECT ON amie.* TO "accounting-api";
```

- [x] Look at OpenAPI
- [x] Return appropriate error if project cannot be found in amie DB
- [x] Figure out _where_ this will run. The Galera cluster does not expose connectivity to the outside world, only to `172.16.0.0/22` on the Jetstream2 control plane.

> 2:59 PM Chris Martin any preference of how the Accounting API backend is packaged/distributed and where on the infrastructure it runs?
> 3:00 PM Mike Lowe Haven’t given it much thought
> 3:06 PM Chris Martin left to my own devices, I would run it on a Docker container on both of the control plane nodes, and let HAProxy could terminate TLS for it
> 3:07 PM would defer to how you want it done, but I don't want my dependencies to pollute the Python environment for anything important
> 3:08 PM Mike Lowe sounds reasonable to me

- [x] get Docker container working
- [ ] Figure out logging in production. Docker logs should be written to disk somewhere
- [x] Query amie database for information about a user's allocation, and return it to the user
- [x] Need a Keystone service account for this so that I'm not using `admin` user
  - Done, it is `accounting-api`
- [x] accept unscoped token and return list of all allocations that a user has access to
- [x] deploy accounting API on j2ctl1
- [x] configure haproxy
