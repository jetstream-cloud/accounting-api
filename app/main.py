from flask import Flask, Blueprint, request
from flask_keystone import FlaskKeystone
from flask_oslolog import OsloLog
from oslo_config import cfg
import records
import sys
from keystoneauth1.identity import v3
from keystoneauth1 import session
from keystoneclient.v3 import client

key = FlaskKeystone()
log = OsloLog()

my_bp = Blueprint("my_bp", __name__)

# Process config file

db_grp = cfg.OptGroup('database')
db_opts = [cfg.StrOpt('connection')]
cfg.CONF.register_group(db_grp)
cfg.CONF.register_opts(db_opts, group=db_grp)

keystone_grp = cfg.OptGroup('keystone_client')
keystone_opts =\
    [cfg.StrOpt('auth_url'),
     cfg.StrOpt('username'),
     cfg.StrOpt('user_domain_id'),
     cfg.StrOpt('password'),
     cfg.StrOpt('project_name'),
     cfg.StrOpt('project_domain_id')
     ]
cfg.CONF.register_group(keystone_grp)
cfg.CONF.register_opts(keystone_opts, group=keystone_grp)

cfg.CONF(sys.argv[1:])


keystone_auth = v3.Password(
    auth_url=cfg.CONF.keystone_client.auth_url,
    username=cfg.CONF.keystone_client.username,
    user_domain_id=cfg.CONF.keystone_client.user_domain_id,
    password=cfg.CONF.keystone_client.password,
    project_name=cfg.CONF.keystone_client.project_name,
    project_domain_id=cfg.CONF.keystone_client.project_domain_id
)
keystone_session = session.Session(auth=keystone_auth)
keystone_client = client.Client(session=keystone_session)


@my_bp.route('/')
def index():
    def get_allocations_for(project_names):
        db = records.Database(cfg.CONF.database.connection)
        ugly_list = ["'{}'".format(i) for i in project_names]
        query = \
            '''
            SELECT
                projects.charge_number AS title,
                projects.project_title AS description,
                projects.abstract AS abstract,
                projects.resource AS resource,
                projects.service_units_allocated,
                balances.used AS service_units_used,
                projects.start_date AS start_date,
                projects.end_date AS end_date,
                projects.active
            FROM projects
            LEFT JOIN balances ON projects.id = balances.project_record_id
            WHERE charge_number IN ({})
            ''' \
                .format(', '.join(ugly_list))
        allocation_rows = db.query(query)
        return allocation_rows.export('json')

    project_name = request.environ.get("HTTP_X_PROJECT_NAME")

    if project_name is not None:
        # Project-scoped token, pass single project name
        return get_allocations_for([project_name])
    else:
        # Non-project-scoped token, get all projects that the user has access to
        user_id = request.environ.get("HTTP_X_USER_ID")
        if user_id is not None:
            projects = keystone_client.projects.list(user=user_id)
            project_names = [project.name for project in projects]

            return get_allocations_for(project_names)
        else:
            return 'Could not determine project or user {}'.format(project_name), 500


def create_app(app_name):
    config_file = "accounting-api.conf"
    conf = cfg.CONF
    conf(default_config_files=[config_file])
    app_ = Flask(app_name)
    log.init_app(app_)
    key.init_app(app_)
    app_.register_blueprint(my_bp)
    return app_


app = create_app(app_name=__name__)

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5000, threaded=True)