FROM tiangolo/uwsgi-nginx-flask:latest
RUN apt-get update && apt-get install -y python3-cryptography
COPY ./requirements.txt /var/www/requirements.txt
RUN pip install --no-cache-dir --upgrade -r /var/www/requirements.txt
COPY ./app /app
COPY accounting-api.conf /app/
